<?php

/**
 * @file
 * Remove all data from the key_value table after uninstall.
 */

/**
 * Implements hook_uninstall().
 *
 * Removing all variables, which has been set via this module.
 */
function list_formatter_uninstall() {
  \Drupal::service('database')
    ->delete('key_value')
    ->condition('name', 'list_formatter')
    ->execute();
}
