<?php

namespace Drupal\list_formatter\Tests;

/**
 * Test the UI settings form for list fields.
 *
 * @group list_formatter
 */
class UITest extends TestBase {

  /**
   * Gets info.
   */
  public static function getInfo() {
    return [
      'name' => 'Test list UI',
      'description' => 'Tests the  settings in the UI for list formatters.',
      'group' => 'List formatter',
    ];
  }

  /**
   * Test the general output of the display formatter.
   */
  public function testUi() {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/structure/types/manage/' . $this->contentType->type . '/display');
    $this->assertSession()->statusCodeEquals(200);
    // Verify the assertion: pageTextContains() for HTML responses,
    // responseContains() for non-HTML responses. The passed text should
    // be HTML decoded, exactly as a human sees it in the browser.
    $this->assertSession()->pageTextContains('Unordered HTML list (ul)');
    $this->assertSession()->pageTextContains('CSS Class: list-formatter-list');
  }

}
