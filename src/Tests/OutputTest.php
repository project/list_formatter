<?php

namespace Drupal\list_formatter\Tests;

use Drupal\Core\Language\Language;

/**
 * Test the rendered output of list fields.
 *
 * @group list_formatter
 */
class OutputTest extends TestBase {

  /**
   * Gets info.
   */
  public static function getInfo() {
    return [
      'name' => 'Test list output',
      'description' => 'Tests the output markup of List list formatters.',
      'group' => 'List formatter',
    ];
  }

  /**
   * Test the general output of the display formatter.
   */
  public function testOutput() {
    $this->drupalLogin($this->adminUser);

    $field_values = [Language::LANGCODE_NOT_SPECIFIED => []];
    for ($i = 0; $i < 10; $i++) {
      $field_values[Language::LANGCODE_NOT_SPECIFIED][] = ['value' => $this->randomName()];
    }

    $node = $this->drupalCreateNode([
      $this->fieldName => $field_values,
      'type' => $this->contentType->type,
    ]);
    $page = $this->drupalGet('node/' . $node->nid);

    $this->drupalSetContent($page);
    $this->assertSession()->statusCodeEquals(200);

    foreach ($field_values[Language::LANGCODE_NOT_SPECIFIED] as $delta => $item) {
      // Verify the assertion: pageTextContains() for HTML responses,
      // responseContains() for non-HTML responses. The passed text should be
      // HTML decoded, exactly as a human sees it in the browser.
      $this->assertSession()->pageTextContains($item['value']);
    }

    $items = [];
    foreach ($field_values[Language::LANGCODE_NOT_SPECIFIED] as $item) {
      $items[] = $item['value'];
    }

    // Test the default ul list.
    $options = [
      'type' => 'ul',
      'items' => $items,
      'attributes' => [
        'class' => ['list-formatter-list'],
      ],
    ];
    $expected = theme('item_list', $options);

    $this->assertSession()->responseContains($expected);

    $entityDisplayRepository = \Drupal::service('entity_display.repository');
    // Update the field settings for ol list.
    $display = $entityDisplayRepository->getViewDisplay('node', $this->contentType->type, 'default');
    $field = $display->getComponent($this->fieldName);
    $field['settings']['type'] = 'ol';
    $display->setComponent($this->fieldName, $field)->save();

    // Get the node page again.
    $this->drupalGet('node/' . $node->nid);

    // Test the default ol list.
    $options['type'] = 'ol';
    $expected = theme('item_list', $options);

    $this->assertSession()->responseContains($expected);

    // Update the field settings for comma list.
    $field = $display->getComponent($this->fieldName);
    $field['settings']['type'] = 'comma';
    $display->setComponent($this->fieldName, $field)->save();

    // Get the node page again.
    $this->drupalGet('node/' . $node->nid);

    // Test the default comma list.
    unset($options['type']);
    // Get the field formatter plugin to pass into the theme function.
    $options['formatter'] = $entityDisplayRepository->getViewDisplay('node', $this->contentType->type, 'default')->getFormatter($this->fieldName);

    $expected = theme('list_formatter_comma', $options);

    $this->assertSession()->responseContains($expected);
  }

}
