<?php

namespace Drupal\list_formatter\Plugin\list_formatter;

/**
 * Plugin implementation of the 'number' list formatter.
 *
 * @ListFormatter(
 *   id = "number",
 *   module = "number",
 *   field_types = {"number_integer", "number_decimal", "number_float"}
 * )
 */
class NumberList extends DefaultList {

}
