<?php

namespace Drupal\list_formatter\Plugin\list_formatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\list_formatter\Plugin\ListFormatterListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_reference' list formatter.
 *
 * @ListFormatter(
 *   id = "entity_reference",
 *   module = "entity_reference",
 *   field_types = {"entity_reference"},
 *   settings = {
 *     "entity_reference_link" = "1"
 *   }
 * )
 */
class EntityReferenceList implements ListFormatterListInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new EntityReferenceList object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createList(FieldItemListInterface $items, FieldDefinitionInterface $field_definition, FormatterInterface $formatter, $langcode) {
    // Load the target type for the field instance.
    $target_type = $field_definition->getSetting('target_type');
    $contrib_settings = $formatter->getSetting('list_formatter_contrib');
    $list_items = $target_ids = $target_entities = [];

    // Get an array of entity ids.
    foreach ($items as $delta => $item) {
      $target_ids[] = $item->target_id;
    }

    // Load them all.
    if ($target_ids) {
      $target_entities = $this->entityTypeManager->getStorage($target_type)->loadMultiple($target_ids);
    }

    // Create a list item for each entity.
    foreach ($target_entities as $id => $entity) {
      // Only add entities to the list that the user will have access to.
      if ($entity->access('view')) {
        $label = $entity->label();
        if ($contrib_settings['entity_reference_link']) {
          $url = $entity->toUrl();

          $target_type_class = Html::getClass($target_type);
          $classes = [
            $target_type_class,
            $target_type_class . '-' . $id, 'entity-reference',
          ];

          $list_items[$id] = [
            '#type' => 'link',
            '#title' => $label,
            '#url' => $url,
            '#options' => [
              'attributes' => [
                'class' => $classes,
              ],
            ],
          ];
        }
        else {
          $list_items[$id] = [
            '#markup' => $label,
            '#allowed_tags' => FieldFilteredMarkup::allowedTags(),
          ];
        }
      }
    }

    return $list_items;
  }

  /**
   * {@inheritdoc}
   */
  public function additionalSettings(&$elements, FieldDefinitionInterface $field_definition, FormatterInterface $formatter) {
    if ($field_definition->getType() == 'entity_reference') {
      $settings = $formatter->getSetting('list_formatter_contrib');
      $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
      $entity_type = $this->entityTypeManager->getDefinition($target_type);
      $elements['list_formatter_contrib']['entity_reference_link'] = [
        '#type' => 'checkbox',
        '#title' => t('Link list items to their @entity_type entity.', ['@entity_type' => strtolower($entity_type->getLabel())]),
        '#description' => t('Generate item list with links to the node page'),
        '#default_value' => $settings['entity_reference_link'],
      ];
    }
  }

}
