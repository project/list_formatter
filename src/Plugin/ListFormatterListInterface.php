<?php

namespace Drupal\list_formatter\Plugin;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterInterface;

/**
 * List formatter list interface.
 */
interface ListFormatterListInterface {

  /**
   * Creates a list from field items.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   Items.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   * @param \Drupal\Core\Field\FormatterInterface $formatter
   *   Formatter object.
   * @param mixed $langcode
   *   Langcode.
   *
   * @return mixed
   *   Returns render array of the list.
   */
  public function createList(FieldItemListInterface $items, FieldDefinitionInterface $field_definition, FormatterInterface $formatter, $langcode);

  /**
   * Provides additional list settings.
   *
   * @param mixed $elements
   *   Array of elements.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Field definition.
   * @param \Drupal\Core\Field\FormatterInterface $formatter
   *   Formatter object.
   *
   * @return mixed
   *   Returns array with additional settings.
   */
  public function additionalSettings(&$elements, FieldDefinitionInterface $field_definition, FormatterInterface $formatter);

}
