<?php

namespace Drupal\list_formatter\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a List Formatter plugin.
 *
 * @Annotation
 */
class ListFormatter extends Plugin {

  /**
   * List of field types.
   *
   * @var array
   */
  public $field_types = [];

  /**
   * Settings array.
   *
   * @var array
   */
  public $settings = [];

  /**
   * Module.
   *
   * @var mixed
   */
  public $module;

}
